
TARGET_DIR = /usr/local/bin
TARGETS =
TARGETS += powermenu.sh
TARGETS += media-postprocess.sh

all:

${TARGET_DIR}/%: %
	cp $< $@

getbg: getbg.c
	$(CC) -Wall -ansi -pedantic $< -o $@ -lX11 -ljpeg


.PHONY: install
install: $(patsubst %,${TARGET_DIR}/%,${TARGETS})
