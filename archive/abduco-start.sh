#! /bin/bash
# NOTE: I no longer use this script.

_abduco () {
    abduco | tail -n +2 | sed 's/^\([*+ ]\).*\s\(.*\)$/\2\1/g'
}

_next_session () {
    # Collect all the session numbers and sort them
    VALS=($(sed 's/ /\n/g' | sed 's/\*//g' | grep 'sess' | \
    grep -v '\+$' | sed 's/sess//g' | sort -b -g))
    # Find a missing number
    for (( i = 0 ; i < $((${#VALS[@]}-1)) ; i++ )); do
    if [ $((${VALS[$i]}+1)) -ne ${VALS[$(($i + 1))]} ] ;then
        NEXT="$((${VALS[$i]}+1))"
        break
    fi
    done
    # Start a session
    if [[ -z $NEXT ]]; then
        if [[ ${#VALS[@]} -eq 0 ]]; then
            echo "sess0"
        else
            if [[ ${VALS[0]} -ne 0 ]]; then
                echo "sess0"
            else
                echo "sess"$(($i+1))
            fi
        fi
    else
        echo "sess"${NEXT[0]}
    fi
}

SESS=$(_abduco)

ATT=$(echo $SESS | sed 's/ /\n/g' | grep -v "\*$" | head -1 | sed 's/\+$//')
NEXT=$(echo $SESS | _next_session)

if [[ -z $ATT ]]; then
    ABDUCO_SESSION=$NEXT exec abduco -c $NEXT zsh
else
    exec abduco -a $ATT
fi
