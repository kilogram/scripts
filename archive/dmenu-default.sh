#!/bin/sh
# Install as /usr/local/bin/dmenu
# Requires dmenu2

function offset {
    python <<EOF
import re

MATCH_CONNECTED = re.compile(
    "^([a-zA-Z0-9-_]+) connected(?: primary)? ([0-9x+]+) .*$",
    flags=re.MULTILINE)
MATCH_WIN = re.compile(".*Position: (\d+),(\d+).*Geometry: (\d+)x(\d+)", re.S)
MATCH_RES = re.compile("(\d+)x(\d+)\+(\d+)\+(\d+)")

WINDOW = """$(xdotool getwindowgeometry $(xdotool getwindowfocus))"""
MONITORS = """$(xrandr)"""

wx, wy, ww, wh = (int(i) for i in MATCH_WIN.match(WINDOW).groups())
screen = 0
matches = MATCH_CONNECTED.findall(MONITORS)
for mon, res in matches:
    screen += 1
    w, h, x, y = (int(i) for i in MATCH_RES.match(res).groups())
    if (wx >= x and wy >= y and (wx) <= (x + w) and (wh) <= (y + h)):
        print("-s %d -x %d -y %d\n" % (len(matches) - screen - 1,
                                       int((w - $1) / 2),
                                       int((h - (20 * $2)) / 2)))
        break
else:
    print("")

EOF
}

width=500; lines=10
systemctl --user --quiet is-active compton && dim="-dim .7"
/usr/bin/dmenu "$@" -i $dim -fn "Inconsolata-12" \
    -nb "#680000" -nf "#daa520" -sb "#daa520" -sf "#680000" \
    $(offset $width $lines) -w $width -l $lines <&0
