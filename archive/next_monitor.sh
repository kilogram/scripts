#!/bin/sh

exec ~/.config/screenlayout/$(find ~/.config/screenlayout -name "*.sh" -printf "%P\n" | cut -f1 -d'.' | dmenu).sh
