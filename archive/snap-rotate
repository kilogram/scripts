#!/bin/bash
# NOTE: I no longer use this script.


print_usage_string () {
    echo -n "Usage: snap-rotate [OPTIONS] TARGET
Establish time hierarchy of snapshots

  -d, --dry-run     Don't perform any modifications
  -m, --monthly NUM How many backups to keep from the past month
  -w, --weekly NUM  How many backups to keep from the past week
  -y, --yearly NUM  How many backups to keep from the past year
  -v, --verbose     Enable verbose output
  -h, --help        display this help and exit
"
}

_echo () { (( $VERBOSE )) && echo $@ >&2 ; }

CMD="bash -c"
MONTHLY=4 ; WEEKLY=$(( 7 * 12 )); YEARLY=12
VERBOSE=0
TEMP=$( getopt -o dm:w:y:vh -l dry-run,monthly:,weekly:,yearly:,verbose,help \
    -n 'snap-rotate' -- "$@" )
eval set -- "$TEMP"
while true ; do
    case $1 in
        -d|--dry-run)
            CMD="echo -n" ; shift 1 ;;
        -m|--monthly)
            MONTHLY=$2 ; shift 2 ;;
        -w|--weekly)
            WEEKLY=$2 ; shift 2 ;;
        -y|--yearly)
            YEARLY=$2 ; shift 2 ;;
        -v|--verbose)
            VERBOSE=1; shift 1 ;;
        -h|--help)
            usage ; exit 0 ; shift 1;;
        --) shift 1 ; break ;;
        *) echo "Invalid argument: $1" ; usage ; exit 1 ;;
    esac
done

TARGET=$(readlink -m $1)

if [[ -z $TARGET ]]; then
    echo "No target specified" >&2
    print_usage_string
    exit 1
fi

if [[ ! -d $TARGET ]]; then
    echo "Invalid target" >&2
    exit 1
fi

NOW=$(date +%s)
CUR_WEEKLY=0
CUR_MONTHLY=0
CUR_YEARLY=0

truncate () {
    if (( $"CUR_$1" < $"$1" )); then
        _echo "Keep $2"
        (( CUR_$1 += 1 ))
    else
        _echo "Delete $2"
        $CMD "btrfs sub delete $TARGET/$2"
    fi
}

for name in $(btrfs subvolume list $TARGET | \
                cut -f 9 -d' ' | \
                grep "^$(basename $TARGET)" | \
                xargs -n1 basename | tac); do
    date=$(echo $name | cut -f2- -d'-' | sed 's/\./ /')
    diff=$(( $NOW - $(date -d"$date" +%s) ))
    if (( $diff < 7*24*60*60 )); then # Within the past week
        truncate WEEKLY $name
        continue
    fi; if (( $diff < 30*7*24*60*60 )); then # Within the past month
        truncate MONTHLY $name
        continue
    fi; if (( $diff < 365*7*24*60*60 )); then # Within the past year
        truncate YEARLY $name
        continue
    fi
done

# Usage: sudo ./snap-rotate -d -v -w 1 -m 1 /backup/home
