#!/bin/bash
disp=$(echo $DISPLAY | sed 's/^.*:\([0-9]\+\)$/\1/')
source $HOME/.dbus/session-bus/7f626e7431acf8a8ed6a14aa5797b81d-${disp}
export DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS
$@
