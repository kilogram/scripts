#! /bin/bash

account=${1:-gmail}
export NOTMUCH_CONFIG=~/.mutt/notmuch-${account}
count=$(notmuch count tag:notify AND tag:inbox)
[[ -z "$count" ]] && exit

message="$count new email"
if (( $count > 1 )); then
    message="${message}s"
fi
if (( $count > 0 )); then
    notify-send \
        "$account: $message"
fi
while ! notmuch tag -notify tag:notify; do sleep 1; done
