#! /bin/bash

ICON_PATH=/usr/share/icons/Adwaita/32x32

function get_state { acpi -b | awk '{print $3}'; }
function get_level { acpi -b | sed 's/.* \([0-9]\+\)%.*/\1/g'; }
function get_time { acpi -b | awk '{print $5}'; }

case $(get_state) in
    Discharging,)
        case $(get_level) in
            [0-4])
                shutdown now; exit
                ;;
            [5-9])
                notify-send "Battery critical" "Time left: $(get_time)"
                ;;
            1[0-9])
                notify-send "Battery low" "Time left: $(get_time)"
                ;;
        esac
        ;;
    Full,)
        notify-send "Battery full"
        while [[ $(get_state) = "Full," ]]; do sleep 60; done
        ;;
    *)
        exit
        ;;
esac
