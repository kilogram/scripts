#!/usr/bin/env python3

import inotify.adapters
import os
from pathlib import Path
import re
import subprocess


ROOT = Path('/tmp/onfiledo')
WATCH_PATHS = []


def watching(regex, actions=()):
    def wrap_watching(func):
        _regex = re.compile(regex)
        WATCH_PATHS.append((_regex, actions, func))
        return func
    return wrap_watching


@watching('ping')
def ping(path, action):
    with open(str(ROOT / Path('pong')), 'w'):
        pass
    return True


@watching('keyboard')
def hotplug_keyboard(path, action):
    try:
        output = subprocess.check_output(["xmodmap", "-"], input="""
clear lock
clear control
keycode 0x42 = Control_L
add control = Control_L Control_R
clear mod1
clear mod4
keycode 0x40 = Super_L
keycode 0x85 = Alt_L
add mod1 = Alt_L Alt_R Meta_L
add mod4 = Super_L Super_R Hyper_L
""".encode())
    except subprocess.CalledProcessError as cpe:
        print(cpe)
        print(output)
    finally:
        return True


def _main():
    try:
        os.mkdir(str(ROOT), mode=0o770)
    except FileExistsError:
        pass

    i = inotify.adapters.InotifyTree(str(ROOT))

    for event in i.event_gen(yield_nones=False):
        (_, type_names, path, filename) = event
        path = Path(path)
        filename = Path(filename)

        for regex, actions, func in WATCH_PATHS:
            actions = actions or ('IN_CLOSE_WRITE',)
            relative = (path / filename).relative_to(ROOT)
            if set(type_names) & set(actions) and regex.match(str(relative)):
                print("events {} at path '{}' matched {} at {}".format(
                      type_names, str(relative), actions, regex))
                try:
                    remove = func(relative, type_names)
                    if remove:
                        os.remove(str(ROOT / relative))
                except Exception as e:
                    print(e)
                    break

if __name__ == '__main__':
    _main()
