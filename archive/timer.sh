#! /bin/bash

function usage {
    echo "Usage: timer [OPTIONS] TIME
Script for setting timers

TIME can be:
- 24-hour time (e.g., 15:25:00 or 15:25)
- 12-hour time (e.g., 8:00pm or 8:00p)
- offset in minutes (300m or 5:00m)
- offset in time (e.g., +1:30 or +90).

Options:
  -v, --verbose     Enable verbose output
  -h, --help        display this help and exit"
}

VERBOSE=0
TEMP=$( getopt -o vh -l verbose,help -n 'timer.sh' -- "$@" )
eval set -- "$TEMP"
while true ; do
    case $1 in
        -v|--verbose)
            VERBOSE=1; shift 1 ;;
        -h|--help)
            usage ; exit 0 ; shift 1;;
        --) shift 1 ; break ;;
        *) echo "Invalid argument: $1" ; usage ; exit 1 ;;
    esac
done

function _echo {
    (( $VERBOSE )) && echo "$@" >&2
}

function delay_to_time {
    _echo "Offset is $1 seconds"
    date -d"today + $1 seconds" +%H:%M:%S
}

function date_subtract {
    _echo "Reference date is today $1"
    echo $(( $(date -d "today $1" +%s) - $(date +%s) ))
}

function parse_time {
    _echo "Parsing $1"
    total=0
    arg=$1
    IFS=":" read -a time <<< "$arg"
    for t in ${time[@]}; do total=$(( $total*60 + $t )); done
    _echo $total
    echo $total
}

function delay {
    sleep $1
    notify-send "Timer ended" "$2"
    echo -n $'\a'
}

if [[ $1 =~ ^- ]]; then
    TIME=""
else
    TIME=$1; shift 1
fi

# Check the last two characters of the argument
case ${TIME:$((${#TIME}-2))} in
    # If it's [am]p?, we can use date_subtract
    am|pm)
        _echo "Time specified in am/pm format"
        delay=$(date_subtract $TIME)
        ;;
    *a|*p)
        _echo "Time specified in am/pm format"
        delay=$(date_subtract ${TIME}m)
        ;;
    # If it's in minutes (appended m) we use parse_time and multiply by 60
    *m)
        _echo "Time specified in minutes"
        delay=$(( $(parse_time ${TIME:0:$((${#TIME}-1))}) * 60 ))
        ;;
    # If it's relative (prepended +) use parse_time; otherwise, it's 24-hr
    *)
        if [[ ${TIME:0:1} = "+" ]]; then
            _echo "Time specified as relative"
            delay=$(parse_time $TIME)
        else
            _echo "Time specified in 24-hour format"
            delay=$(date_subtract $TIME)
        fi
        ;;
esac

_echo "Timer going off at $(delay_to_time $delay)"
delay $delay $TIME
