#!/bin/sh
action=$(echo -e "blank\nlock\nsuspend\nshutdown\nreboot" | dmenu)
sleep .4
case $action in
    blank)
      lock.sh --no-pause ;;
    lock)
      loginctl lock-session ;;
    suspend)
      systemctl suspend ;;
    shutdown)
      systemctl poweroff ;;
    reboot)
      systemctl reboot ;;
    *) ;;
esac
