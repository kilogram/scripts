#!/bin/bash

set -o nounset
set -o errexit

exec >> /var/lib/transmission/media-postprocess-last.log 2>&1

print_usage_string () {
    echo -n "Usage: media-postprocess.sh DOWNLOAD [OPTIONS]
Add downloaded media to Plex appropriately

  --type        Import media as movie/tv/anime/music. If not specified, automatic classification is attempted.

In order for this script to be used with transmission, it uses the environment variables TR_TORRENT_DIR and TR_TORRENT_NAME.
"
}

DL_DIR=/bulk0/dl
DEST_DIR=/bulk0/media
LABELS=( movie tv anime music )

TEMP=$( getopt -o n:,t: -l name:,type:,help -n 'media-postprocess.sh' -- "$@" )

eval set -- "$TEMP"
while true ; do
    case $1 in
        -t|--type)
            TYPE=$2 ; shift 2 ;;
        --help)
            print_usage_string ; exit 0 ; shift 1 ;;
        --) shift 1 ; break ;;
        *) echo "Invalid argument: $1" ; print_usage_string ; exit 1 ;;
    esac
done

if [[ $# -gt 0 ]]; then
    TR_TORRENT_DIR=$(dirname "$1")
    TR_TORRENT_NAME=$(basename "$1")
fi

LABEL_GUESS=$(basename ${TR_TORRENT_DIR:-""})
[[ ${LABELS[@]} =~ $LABEL_GUESS ]] && LABEL_GUESS=""
LABEL=${TYPE:-${LABEL_GUESS}}
[[ -z $LABEL ]] || LABEL="ut_label=$LABEL"

[[ ! -v TR_TORRENT_DIR ]] || [[ ! -v TR_TORRENT_NAME ]] || \
    [[ -z $TR_TORRENT_DIR ]] || [[ -z $TR_TORRENT_NAME ]] && \
    { echo "Either DOWNLOAD or the TR_TORRENT_DIR and TR_TORRENT_NAME environment variables must be specified"; print_usage_string; exit 1; }

MOVIE_FORMAT="Movies/{n} ({y})/{fn}"
SERIES_FORMAT="TV/{n}/Season {s}/{n} - {s00e00} - {t}"
ANIME_FORMAT="Anime/{n}/Season {s}/{n} - {s00e00} - {t}"
MUSIC_FORMAT="Music/{n}/{fn}"

echo -n "Running filebot for $LABEL SOURCE: $TR_TORRENT_NAME DEST: $DEST_DIR -- "

umask 002

filebot -script fn:amc --output "$DEST_DIR" --action duplicate --conflict skip \
    -non-strict --log-file /tmp/amc.log \
    --def excludeList=amc.excludes unsorted=y music=y artwork=y \
    "ut_kind=multi" \
    "ut_dir=$TR_TORRENT_DIR/$TR_TORRENT_NAME" \
    "ut_title=$TR_TORRENT_NAME" \
    $LABEL \
    "movieFormat=$MOVIE_FORMAT" \
    "seriesFormat=$SERIES_FORMAT" \
    "animeFormat=$ANIME_FORMAT" \
    "musicFormat=$MUSIC_FORMAT" \
    # "pushbullet=<secret_key>" \
    "plex=localhost:quCqEHpoFmCpszUUZG1t" \
    "plex=localhost:hYc9tuzietkxTZHMVzt3" \
    && echo " successful." || echo "failed."
