#!/bin/bash

print_usage_string () {
    echo -n "Usage: dock.sh [OPTIONS] [dock|undock]
Quickly dock or undock laptop

  -v, --verbose     Enable verbose output
  -h, --help        display this help and exit
"
}

VERBOSE=0
TEMP=$( getopt -o vh -l verbose,help \
    -n 'dock' -- "$@" )

eval set -- "$TEMP"
while true ; do
    case $1 in
        -v|--verbose)
            VERBOSE=1; shift 1 ;;
        -h|--help)
            print_usage_string ; exit 0 ; shift 1;;
        --) shift 1 ; break ;;
        *) echo "Invalid argument: $1" ; print_usage_string ; exit 1 ;;
    esac
done

ACTION=${1-:dock}

case $ACTION in
    dock)
        next_monitor.sh --preserve &
        quickmount dock &&
        notify-send "Dock" "devices mounted"
        ;;
    undock)
        sh ${XDG_CONFIG_HOME:-$HOME/.config}/screenlayout/1-h.sh &
        quickmount undock &&
        notify-send "Dock" "devices unmounted"
        ;;
    *)
        echo "Invalid action $ACTION" >&2
        print_usage_string
        exit 1
        ;;
esac
