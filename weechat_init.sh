#!/bin/sh
# A (hopefully) idempotent means of configuring weechat portably

set -o errexit
set -o pipefail
set -o nounset

THIS_FILE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )/$( basename "${BASH_SOURCE[0]}" )"
FROM_WEECHAT="--from-weechat"

filter () {
    FILE=${1:-}
    COMMAND=${2:-}
    [[ -z $FILE ]] && { echo "filter: no file specified" 2>&1; return 1; }
    sed -E -n -e "/BEGIN(disambiguate)? weechat only/,/END weechat only/{
            s/^#.*$//;
            /weechat only/d;
            /^\s*$/d;
            ${COMMAND}
        }" ${FILE}
}

help () {
    echo "This script is intended to be run from within weechat."
    echo "Copy-paste the following commands:"
    echo
    filter ${THIS_FILE} '/^\/(secure|script)/p;'
    echo
    echo /exec -oc ${THIS_FILE} ${FROM_WEECHAT}
    exit 0
}

if [ $# -eq 0 ]; then
    echo "Installing wee-slack..."
    mkdir -p ~/.weechat/python/autoload
    wget https://raw.githubusercontent.com/wee-slack/wee-slack/master/wee_slack.py
    cp wee_slack.py ~/.weechat/python/autoload
    echo
    help
else
    case $1 in
    -h|--help)
        help
        ;;
    $FROM_WEECHAT)
        filter ${THIS_FILE} '/^\/(secure|script)/d;p;'
        # We can't evaluate ${THIS_FILE} in the single quoted 'comment' below
        echo "/alias add reload_config /exec -oc ${THIS_FILE} ${FROM_WEECHAT}"
        ;;
    *)
        echo "Unrecognized argument; try --help" >&2
        exit 1
        ;;
    esac
fi

exit 0
# We use the true alias ':' with single quoted string literal to avoid variable
# interpolation.
: ' BEGIN weechat only
# haha
/alias add SHRUG "input send ¯\\_(ツ)\_/¯"

/set irc.server_default.nicks kilogram
/set weechat.plugin.autoload "*,!lua,!tcl,!fifo,!xfer,!guile,!javascript"

/set spell.check.default_dict en
/set spell.check.suggestions 3
/set spell.color.suggestion *green
/spell enable

# Do not merge server buffers with core buffer
/set irc.look.server_buffer independent

/set buflist.format.buffer "${format_number}${indent}${eval:${format_name}}${format_hotlist} ${color:green}${buffer.local_variables.filter}${buffer.local_variables.buflist}"
/set buflist.format.buffer_current ${if:${type}==server || ${type}==?${color:*white,lightcyan}:${color:*white,green}}${hide:>,${buffer[last_gui_buffer].number}} ${indent}${if:${type}==server&&${info:irc_server_isupport_value,${name},NETWORK}?${info:irc_server_isupport_value,${name},NETWORK}:${name}} ${color:green}${buffer.local_variables.filter}${buffer.local_variables.buflist}
/set buflist.format.hotlist " ${color:darkgray}${hotlist}${color:darkgray}"
/set buflist.format.hotlist_highlight "${color:lightcyan}"
/set buflist.format.hotlist_message "${color:lightcyan}"
/set buflist.format.hotlist_private "${color:lightblue}"
/set buflist.format.indent "${if:${type}==channel&&${buffer.name}=~fr$||${info:spell_dict,${buffer.full_name}}=~fr?${color:blue}f :  }${color:*white}"
/set buflist.format.name "${if:${type}==server?${color:white}:${color_hotlist}}${if:${type}==server||${type}==channel||${type}==private?${if:${cutscr:8,+,${name}}!=${name}?${cutscr:8,${color:${weechat.color.chat_prefix_more}}+,${if:${type}==server&&${info:irc_server_isupport_value,${name},NETWORK}?${info:irc_server_isupport_value,${name},NETWORK}:${name}}}:${cutscr:8, ,${if:${type}==server&&${info:irc_server_isupport_value,${name},NETWORK}?${info:irc_server_isupport_value,${name},NETWORK}                              :${name}                              }}}:${name}}"
/set buflist.format.number "${if:${type}==server?${color:black}:${color:darkgray}}${number}${if:${number_displayed}?.: }"

/set weechat.bar.buflist.size 18
/set weechat.bar.buflist.size_max 18

# Different title bars for active/inactive windows
/bar add activetitle window top 1 0 buffer_title
/set weechat.bar.activetitle.priority 500
/set weechat.bar.activetitle.conditions "${active}"
/set weechat.bar.activetitle.color_fg white
/set weechat.bar.activetitle.color_bg red
/set weechat.bar.activetitle.separator on

/set weechat.bar.title.conditions "${inactive}"
/set weechat.bar.title.color_fg white
/set weechat.bar.title.color_bg darkgray

# Replace status bar with one in the root (one for all windows)
/bar add rootstatus root bottom 1 0 [time],[buffer_count],[buffer_plugin],buffer_number+:+buffer_name+(buffer_modes)+{buffer_nicklist_count}+buffer_filter,[bitlbee_typing_notice],[lag],[spell_dict],[spell_suggest],completion,scroll
/set weechat.bar.rootstatus.color_fg green
/set weechat.bar.rootstatus.color_bg red
/set weechat.bar.rootstatus.separator on
/set weechat.bar.rootstatus.priority 500
/bar del status
/bar set rootstatus name status

# Replace input bar with one in the root
/bar add rootinput root bottom 1 0 [buffer_name]+[input_prompt]+(away),[input_search],[input_paste],input_text
/set weechat.bar.rootinput.color_bg black
/set weechat.bar.rootinput.priority 1000
/bar del input
/bar set rootinput name input
/set weechat.bar.input.items "[mode_indicator]+[input_prompt]+(away),[input_search],[input_paste],input_text"

/set weechat.bar.nicklist.color_fg lightcyan
/set weechat.bar.nicklist.separator on
/set weechat.bar.nicklist.conditions "${nicklist}"
/set weechat.bar.nicklist.size_max 5
/set weechat.bar.nicklist.position top

/set weechat.color.bar_more gray
/set weechat.color.chat_highlight blue
/set weechat.color.chat_highlight_bg green
/set weechat.color.separator green

/set irc.look.smart_filter on
/filter add irc_smart * irc_smart_filter *

/script update

/script install vimode.py
/set plugins.var.python.vimode.no_warn on
/vimode bind_keys
/set plugins.var.python.vimode.imap_esc jk
/set vimode.mode_indicator_normal_color "white"
/set vimode.mode_indicator_normal_color_bg "red"
/set vimode.mode_indicator_insert_color "red"
/set vimode.mode_indicator_insert_color_bg "white"
/set vimode.mode_indicator_cmd_color "white"
/set vimode.mode_indicator_cmd_color_bg "magenta"
/set vimode.mode_indicator_replace_color "white"
/set vimode.mode_indicator_replace_color_bg "magenta"
/set vimode.mode_indicator_search_color "white"
/set vimode.mode_indicator_search_color_bg "magenta"

/script install go.py
/script install toggle_nicklist.py

# Idempotent keys
/key resetall -yes
/key bind ctrl-G "/input switch_active_buffer_previous"
/key bind ctrl-H "/input switch_active_buffer"
/key bind ctrl-J "/buffer +1"
/key bind ctrl-K "/buffer -1"
/key bind ctrl-L "/buffer clear"
/key bind ctrl-N "/bar toggle nicklist"
' # END weechat only
