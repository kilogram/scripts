#!/bin/bash

getbg /tmp/screen.jpg 
convert /tmp/screen.jpg -scale 5% -scale 2000% /tmp/screen.png

PLAYING=0
function prelock {
    xautolock -disable
    xset +dpms dpms 5 5 5
    pkill -u "$USER" -SIGUSR1 dunst
    [[ $1 = '--no-pause' ]] && return
    [[ $(playerctl status) = "Playing" ]] && PLAYING=1
    playerctl pause &
}

function postlock {
    pkill -u "$USER" -SIGUSR2 dunst
    xset dpms 0 0 0
    xautolock -enable
    (( $1 )) && playerctl play
    rm /tmp/screen.{jpg,png}
}

trap postlock SIGHUP SIGINT SIGTERM
prelock $1
locker="i3lock --nofork --image=/tmp/screen.png"
$locker && postlock $PLAYING
