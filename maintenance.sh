#!/bin/sh

# Remove uninstalled packages from the cache
pacman -Sc --noconfirm

# Clear cached packages that are more than two version old
paccache -r -k 2

# Removed orphaned packages
pacman -Rns --noconfirm $(pacman -Qtdq)

# Update the mirrorlist
reflector --protocol http --latest 200 --number 20 --sort rate --country 'United States' --save /etc/pacman.d/mirrorlist

# Update pkgfile database
pkgfile --update
