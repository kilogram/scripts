#!/bin/bash

# Depends on xutils, xdotool, and dmenu

declare -A windows
for window in $(xprop -root | grep "^_NET_CLIENT_LIST\>" | grep -Po "0x[0-9a-f]+"); do
  name=$(xdotool getwindowname $window)
  [[ -z $name ]] && continue
  windows["$name"]=$window
done

selection=$(printf '%s\n' "${!windows[@]}" | dmenu)
[[ -z $selection ]] && exit
xdotool windowactivate ${windows["$selection"]}
